<?php
namespace DeCix\FluidTemplateEditor\Controller;

/*                                                                            *
 * This script belongs to the TYPO3 Flow package "DeCix.FluidTemplateEditor". *
 *                                                                            *
 *                                                                            */

use DeCix\FluidTemplateEditor\Domain\Model\Template;
use DeCix\FluidTemplateEditor\Domain\Repository\TemplateRepository;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use Neos\Flow\Property\TypeConverter\PersistentObjectConverter;
use Neos\FluidAdaptor\View\StandaloneView;

/**
 * Standard controller
 *
 * @Flow\Scope("singleton")
 */
class StandardController extends ActionController {

	/**
	 * @Flow\InjectConfiguration("categories")
	 * @var String[]
	 */
	protected $categories;

	/**
	 * @Flow\Inject
	 * @var TemplateRepository
	 */
	protected $templateRepository;

	/**
	 * Lists the available templates.
	 *
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('templates', $this->templateRepository->findAll());
	}

	/**
	 * Shows a form for creating a new template.
	 *
	 * @return void
	 */
	public function newAction() {
		$this->view->assign('categories', $this->categories);
	}

	/**
	 * Adds the given new template.
	 *
	 * @param Template $template A new template to add
	 * @return void
	 */
	public function createAction(Template $template) {
		$this->templateRepository->add($template);
		$this->addFlashMessage('Template was created.');
		$this->redirect('edit', NULL, NULL, array('template' => $template));
	}

	/**
	 * Shows a form for editing an existing template.
	 *
	 * @param Template $template The template to edit
	 * @return void
	 */
	public function editAction(Template $template) {
		$this->view->assign('previewUrl', $this->uriBuilder->uriFor('preview'));
		$this->view->assign('getPreviewDataUrl', $this->uriBuilder->uriFor('getPreviewData'));
		$this->view->assign('categories', $this->categories);
		$this->view->assign('template', $template);
	}

	/**
	 * Sets up property mapping for the previewAction.
	 *
	 * @return void
	 */
	protected function initializePreviewAction() {
		$configuration = $this->arguments['template']->getPropertyMappingConfiguration();
		$configuration->setTypeConverterOption('Neos\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED, TRUE);
		$configuration->allowProperties('previewData');
	}

	/**
	 * Shows a form for editing an existing template.
	 *
	 * @param Template $template The template to edit
	 * @param string $jsonPreviewData
	 * @return string
	 */
	public function previewAction(Template $template, $jsonPreviewData) {
		$this->response->setHeader('Content-Type', 'text/plain');

		$standaloneView = new StandaloneView();
		$standaloneView->setTemplateSource($template->getSource());

		$standaloneView->assignMultiple(
			$this->transformDottedKeysToNestedArray(json_decode($jsonPreviewData, TRUE))
		);

		try {
			return $standaloneView->render();
		} catch (\Neos\Fluid\Core\Parser\Exception $exception) {
			$this->response->setStatus(500);
			return 'Error rendering preview:' . chr(10) . chr(10) . $exception->getMessage();
		}
	}

	/**
	 * Returns the templates preview data as JSON.
	 *
	 * @param Template $template
	 * @return string
	 */
	public function getPreviewDataAction(Template $template) {
		$this->response->setHeader('Content-Type', 'application/json');
		$previewData = $template->getPreviewData();
		return json_encode($previewData);
	}

	/**
	 * Updates the given template.
	 *
	 * @param Template $template The template to update
	 * @param string $jsonPreviewData
	 * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
	 */
	public function updateAction(Template $template, $jsonPreviewData = '[]') {
		$template->setPreviewData(json_decode($jsonPreviewData, TRUE));

		$this->templateRepository->update($template);
		$this->addFlashMessage('Template was updated.');
		$this->redirect('edit', NULL, NULL, array('template' => $template));
	}

	/**
	 * Removes the given template.
	 *
	 * @param Template $template The template to delete
	 * @return void
	 */
	public function deleteAction(Template $template) {
		$this->templateRepository->remove($template);
		$this->addFlashMessage('Template was deleted.');
		$this->redirect('index');
	}

	/**
	 * Transforms a flat input array with dot notation keys into a nested array.
	 *
	 * @param array $input
	 * @return array
	 */
	protected function transformDottedKeysToNestedArray(array $input) {
		$output = array();
		foreach ($input as list($key, $value)) {
			if (strpos($key, '.') === FALSE) {
				$output[$key] = $value;
				continue;
			}

			$explodedKey = explode('.', $key);
			$pointer =& $output;
			foreach ($explodedKey as $subKey) {
				// If we didn't already turn the thing we're referring to into an array, do so.
				if (!is_array($pointer)) {
					$pointer = array();
				}
				// If the key doesn't exist in our reference, create it as an empty array
				if (!array_key_exists($subKey, $pointer)) {
					$pointer[$subKey] = array();
				}
				// Reset the reference to our new array.
				$pointer = &$pointer[$subKey];
			}
			// Now that we're pointing deep into the nested array, we can
			// set the inner-most value to what it should be.
			$pointer = $value;
		}

		return $output;
	}

	/**
	 * Hide the standard flash message.
	 *
	 * @return boolean
	 */
	protected function getErrorFlashMessage() {
		return FALSE;
	}
}