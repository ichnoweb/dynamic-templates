<?php
namespace DeCix\FluidTemplateEditor\Domain\Model;

/*                                                                            *
 * This script belongs to the TYPO3 Flow package "DeCix.FluidTemplateEditor". *
 *                                                                            *
 *                                                                            */

use Doctrine\ORM\Mapping as ORM;
use Neos\Flow\Annotations as Flow;

/**
 * Template domain model
 *
 * @Flow\Entity
 */
class Template {

	/**
	 * @var String[]
	 * @Flow\InjectConfiguration("categories")
	 * @Flow\Transient
	 */
	protected $categories = array();

	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 */
	protected $name;

	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 */
	protected $category;

	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $source = '';

	/**
	 * @var array
	 */
	protected $previewData = array(array('', ''));

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * @param string $category
	 * @return void
	 */
	public function setCategory($category) {
		$this->category = $category;
	}

	/**
	 * @return string
	 */
	public function getCategoryLabel() {
		return $this->categories[$this->category];
	}

	/**
	 * @return string
	 */
	public function getSource() {
		return $this->source;
	}

	/**
	 * @param string $source
	 * @return void
	 */
	public function setSource($source) {
		$this->source = $source;
	}

	/**
	 * @return mixed
	 */
	public function getPreviewData() {
		return $this->previewData;
	}

	/**
	 * @param mixed $previewData
	 * @return void
	 */
	public function setPreviewData($previewData) {
		$this->previewData = $previewData;
	}

}