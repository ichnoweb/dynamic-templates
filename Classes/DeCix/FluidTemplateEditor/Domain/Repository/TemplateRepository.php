<?php
namespace DeCix\FluidTemplateEditor\Domain\Repository;

/*                                                                            *
 * This script belongs to the TYPO3 Flow package "DeCix.FluidTemplateEditor". *
 *                                                                            *
 *                                                                            */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\QueryInterface;
use Neos\Flow\Persistence\Repository;

/**
 * A repository for Templates
 *
 * @Flow\Scope("singleton")
 */
class TemplateRepository extends Repository {

	/**
	 * @var array
	 */
	protected $defaultOrderings = array('name' => QueryInterface::ORDER_ASCENDING);

}
