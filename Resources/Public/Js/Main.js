Modernizr.load([
	'_Resources/Static/Packages/DeCix.Cws/Js/vendor/jquery.js',
	'_Resources/Static/Packages/DeCix.Cws/Js/vendor/fastclick.js',
	{
		load: '_Resources/Static/Packages/DeCix.Cws/Js/foundation.min.js',
		complete: function() {
			$(document).foundation();
		}
	},{
		load: '_Resources/Static/Packages/DeCix.Cws/Js/vendor/jquery.tablesorter.min.js',
		complete: function() {
			var table = jQuery('.tablesort');
			if (table.length > 0) {
				var headerData = jQuery(table).find('.tablesort_header').clone().removeClass('tablesort_header').addClass('tableSort_infoRow');
				var tableObject;

				function tableInitInfoRows(e) {
					var allRows = tableObject.$tbodies.children('tr');
					allRows.each(function(index, ele) {
						if (index > 0 && index%50 === 0) {
							jQuery(ele).after(headerData.clone());
						}
					});
				}

				function tableDeleteInfoRows(e) {
					tableObject.$tbodies.children('.tableSort_infoRow').remove();
				}

				table.tablesorter({
					selectorHeaders: '.tablesort_header > th',
					sortList: [[0,0]],
					initialized: function(e) {
						tableObject = this;
						tableInitInfoRows(e);
					}
				});
				table.bind('sortStart',tableDeleteInfoRows).bind('sortEnd',tableInitInfoRows);
			}
		}
		
	},
	{
		load: '_Resources/Static/Packages/DeCix.Cws/Js/highcharts_new.js',
		complete: function() {
			Modernizr.load(['_Resources/Static/Packages/DeCix.Cws/Js/highcharts_exporting.js', '_Resources/Static/Packages/DeCix.Cws/Js/highcharts_csv.js']);
		}
	}

]);

Ext.onReady(function(){

	var decix_global = {
		$body: Ext.getBody()
	};

	var ajaxLink = Ext.query('.ajaxLink');
	if(ajaxLink.length>0){
		function callbackFunction(data){
			var modalWindow = document.getElementById('modalWindow').cloneNode(true);
			modalWindow.querySelector('.modalWindow-inner').insertAdjacentHTML('afterbegin',data);

			var modalContent = modalWindow.querySelector('.modalWindow-outer'),
			className = modalContent.className;
			modalContent.className = className+' is-active';
			decix_global.$body.appendChild(modalContent);

			Ext.get(Ext.DomQuery.selectNode('.modalClose')).addListener('click',function(e){
				e.preventDefault();
				var p = Ext.get(this).findParent('.modalWindow-outer');
				p.parentNode.removeChild(p);
			});
		}


		Ext.get(ajaxLink).addListener('click',function(e){
			e.preventDefault();

			var object = this.getAttribute('data-object'),
				method = this.getAttribute('data-method'),
				url = this.getAttribute('href'),
				arguments = [],
				parameters,
				paramPos;

				paramPos = url.indexOf('?');
				parameters = url.substr(paramPos+1);
				parameters = parameters.split('&');

				for(var i=0;i<parameters.length;i++){
					var sp = parameters[i].split('=');
					arguments.push(decodeURIComponent(sp[1]));
				}
				i=null;

				arguments.push(callbackFunction);
				window[object][method].apply(window[object], arguments);
		});


		Ext.get(decix_global.$body).addKeyListener(27,function(e){
			Ext.DomQuery.selectNode('.modalClose').click();
		});
	}

	/* AJAX Delete duplicator */
	Ext.get(decix_global.$body).addListener('click',function(e){
		var target = e.target;
		var confRes = confirm('Are you sure to delete '+target.getAttribute('data-confirm')+'?');
		if(confRes){
			Ext.Ajax.request({
				method: 'POST',
				url: target.getAttribute('href'),
				callback: function(options,success,response){
					if(success === 'true'){
						var p = target.parentNode;
						p.parentNode.removeChild(p);
						alert("Data deleted");
					}else{
						alert("Error while deleting.");
					}
				}
			});
		}else{
			return;
		}
	},null,{
		delegate: '.duplicateDelete',
		preventDefault: true
	});


	Ext.get(decix_global.$body).addListener('click',function(e){
		
		var wrapper = e.getTarget().parentNode,
			count = wrapper.getAttribute('data-count'),
			content = wrapper.querySelector('.duplicatorContent').cloneNode(true),
			fields = content.querySelectorAll('input,select');
		count = parseInt(count)+1;
		wrapper.setAttribute('data-count',count);

		for(var i=0;i<fields.length;i++){
			var field = fields[i];
			field.value = '';
			var fName = field.getAttribute('name');
			fName = fName.replace(/\[\d+\]/g,'['+count+']');
			field.setAttribute('name',fName);
			field.removeAttribute('required');
		}
		content.setAttribute('class','');
		wrapper.appendChild(content);

	},null,{
		delegate: '.duplicate',
		preventDefault: true
	});

	/* AJAX Loader */
	function showAJAXLoader(element){
		element.show();
	}
	function hideAJAXLoader(element){
		element.hide();
	}

	Ext.get(decix_global.$body).addListener('submit',function(e,el){
		var ele = Ext.get(document.getElementById('loaderAnimation'));
		showAJAXLoader(ele);
		window.setTimeout(function(){
			hideAJAXLoader(ele);
		},5000);
	});


	/*
		Load Charts via AJAX
	*/
	(function(){
		function jsonReviver(key, value) {
			if (typeof  value === 'string'
				&& value.indexOf('function(') > -1) {
					value = value.substring(12,value.length-1);
					value = new Function(value);
			}
			return value;
		}

		Ext.Ajax.timeout = 180000;

		function callbackFunction(data,obj) {
			try{
				var res = JSON.parse(data, jsonReviver);
				if(res.chart){
					res.chart.renderTo = this;
				}else{
					res.chart = {
						renderTo: this
					};
				}
				new Highcharts.Chart(res);
			
			}catch(e) {
				var text = "Could not load data.";
				if(obj.type == 'exception'){
					text += ' ';
					text += obj.message;
				}
				this.textContent = text;
			}

		}

		var allChartContainers = document.querySelectorAll('.ajaxChart');
		var chartCount = allChartContainers.length;

		if (chartCount > 0) {
            Ext.get(document).on({
                'webkitfullscreenchange': {
                    fn: function(e){
                        if (document.webkitFullscreenElement) {
                            var chartId = document.webkitFullscreenElement.getAttribute('data-highcharts-chart');
                            decix_global.lastFullscreenChart = Highcharts.charts[chartId];
                        } else {
                            if (decix_global.lastFullscreenChart) {
                                decix_global.lastFullscreenChart.setSize(decix_global.lastFullscreenChart.hiddenOptions.width, decix_global.lastFullscreenChart.hiddenOptions.height, false);
                            }
                        }
                    }
                },
                'mozfullscreenchange': {
                    fn: function(e){
                        if (document.mozFullScreenElement) {
                            var chartId = document.mozFullScreenElement.getAttribute('data-highcharts-chart');
                            decix_global.lastFullscreenChart = Highcharts.charts[chartId];
                        } else {
                            if (decix_global.lastFullscreenChart) {
                                decix_global.lastFullscreenChart.setSize(decix_global.lastFullscreenChart.hiddenOptions.width, decix_global.lastFullscreenChart.hiddenOptions.height, false);
                            }
                        }
                    }
                },
                'fullscreenchange': {
                    fn: function(){
                        if (document.fullscreenElement) {
                            var chartId = document.fullscreenElement.getAttribute('data-highcharts-chart');
                            decix_global.lastFullscreenChart = Highcharts.charts[chartId];
                        } else {
                            if (decix_global.lastFullscreenChart) {
                                decix_global.lastFullscreenChart.setSize(decix_global.lastFullscreenChart.hiddenOptions.width, decix_global.lastFullscreenChart.hiddenOptions.height, false);
                            }
                        }
                    }
                }
            });
		}

		for(var i=0;i<chartCount;i++){
			var ajaxArguments = [];
			ajaxArguments[0] = callbackFunction;

			var element = allChartContainers[i];
			var localCanvas = element.querySelector('.chartContainer');
			ajaxArguments[1] = localCanvas;
			
			var controller = element.getAttribute('data-controller');
			var method = element.getAttribute('data-method');

			var additionalArguments = element.getAttribute('data-arguments');
			if (additionalArguments) {
				var argumentObject = [];

				var indexComma = additionalArguments.indexOf(',');
				var givenArguments = [];
				if (indexComma != -1){
					givenArguments = additionalArguments.split(',');
				} else {
					givenArguments = [additionalArguments];
				}

				givenArguments.reverse();
				for(var j=0; j<givenArguments.length; j++) {
					ajaxArguments.unshift(givenArguments[j]);
				}
			}

			window[controller][method].apply(null,ajaxArguments);
		}
	})();

});